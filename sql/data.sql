INSERT INTO getmorememory.products (id,image,main_description,secondary_description,regular_price,is_available) VALUES
('6c212775-5da2-11e9-943a-e764411abf71','/assets/images/ram.svg','8 GB','DDR4-2400 10-12-10-27 1.65V',2000,1)
,('6f52d136-5da2-11e9-943a-e764411abf71','/assets/images/ram.svg','16 GB','DDR4-2400 10-12-10-27 1.65V',4000,1)
,('72047f86-5da2-11e9-943a-e764411abf71','/assets/images/ram.svg','32 GB','DDR4-2400 10-12-10-27 1.65V',8000,1)
,('764c3d26-5da2-11e9-943a-e764411abf71','/assets/images/ram.svg','64 GB','DDR4-2400 10-12-10-27 1.65V',16000,1)
,('892673a9-5da2-11e9-943a-e764411abf71','/assets/images/harddisk.svg','120 GB','Solid State Driver (SSD)',12000,1)
,('8df7611d-5da2-11e9-943a-e764411abf71','/assets/images/harddisk.svg','240 GB','Solid State Driver (SSD)',24000,1)
,('a61f17c1-5da2-11e9-943a-e764411abf71','/assets/images/cpu.svg','3.2Ghz','LGA1151 socket compatible',24000,1)
,('ad3fff79-5da2-11e9-943a-e764411abf71','/assets/images/cpu.svg','7.4Ghz','LGA1151 socket compatible',48000,1)
,('ed151498-5005-4eb6-ba99-1b1ee4bf052b','/assets/images/ram.svg','4 GB','DDR4-2400 10-12-10-27 1.65V',1000,1)
;