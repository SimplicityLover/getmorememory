package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"html/template"
	"log"
	"net/http"
	"os"
)

type Product struct {
	Id string `gorm:"primary_key;type:CHAR(60)"`
	Image string
	MainDescription string
	SecondaryDescription string
	RegularPrice int
	IsAvailable bool
}

func ToCurrency( value int ) string {
	return "$ " + fmt.Sprintf("%.2f", float32( value ) / 100 )
}

func main() {

	name , error := os.Hostname()

	if error != nil {
		log.Fatal( "Unable to connect to retrieve hostname" )
		os.Exit( 1 )
	}

	host := flag.String( "host" , "mysql.getmorememory.com:3307" , "Db Host" )
	dbName := flag.String( "db" , "getmorememory" , "Db Name" )
	user := flag.String( "user" , "cerouno" , "Db User" )
	password := flag.String( "password" , "cerouno" , "Db Password" )

	flag.Parse()

	connectionString := fmt.Sprintf( "%s:%s@tcp(%s)/%s?charset=utf8" , *user , *password , *host , *dbName )


	db , error := gorm.Open( "mysql" , connectionString )

	if error != nil {
		log.Fatal( error.Error() )
		log.Fatal( "Unable to connect to database" )
		os.Exit( 2 )
	}

	db.AutoMigrate( &Product{} )

	router := gin.Default()

	router.SetFuncMap( template.FuncMap{
		"Currency" : ToCurrency,
	})

	router.LoadHTMLGlob( "templates/*" )

	router.Static("/assets","./public_html")



	router.GET("/", func(context *gin.Context) {
		context.Header( "X-Node" , name )
		products := &[]Product{}
		db.Find( &products )

		context.HTML( http.StatusOK , "index.html" , gin.H{ "Hostname" : name , "Products" : products } )
	})

	router.Run(":9777")

}

